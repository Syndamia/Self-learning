#include <string>

#include "BankAccount.h"

int currId = 0;

BankAccount::BankAccount() {
    id = currId++;
    balance = 0;
}

BankAccount::BankAccount(int initialDeposit) {
    id = currId++;
    balance = initialDeposit;
}


int BankAccount::getId() {
    return id;
}

int BankAccount::getBalance() {
    return balance;
}

bool BankAccount::getCloseStatus() {
    return closed;
}


void BankAccount::deposit(int amount) {
    balance += amount;
}

int BankAccount::withdraw(int amount) {
    if (amount > balance) {
        return -1;
    }
    balance -= amount;
    return amount;
}

std::string BankAccount::toString() {
    return "ID: " + std::to_string(id) + " Balance: " + std::to_string(balance) + ((closed)?" CLOSED":"");
}

void BankAccount::close() {
    closed = true;
}

void BankAccount::reopen() {
    closed = false;
}