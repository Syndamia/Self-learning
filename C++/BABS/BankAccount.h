#ifndef TEST_BANKACCOUNT
#define TEST_BANKACCOUNT
#include <string>

class BankAccount {
    int id, balance;
    bool closed;
    public:
        BankAccount();
        BankAccount(int);
        
        int getId();
        int getBalance();
        bool getCloseStatus();
        
        void deposit(int);
        int withdraw(int);
        std::string toString();
        void close();
        void reopen();
};

#endif