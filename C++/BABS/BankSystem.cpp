#include <iostream>
#include <vector>

#include "BankSystem.h"

using namespace std;

std::vector<BankAccount> BADatabase;

int getIndexFromID(int id) {
    for(int i = 0; i < BADatabase.size(); i++) {
        if (BADatabase[i].getId() == id) {
            return i;
        }
    }
    return -1;
}

void listAllAccounts() {
    for(BankAccount ba: BADatabase) { 
        cout << ba.toString() << endl; 
    }
}

void openBankAccount() {
    cout << "Initial balance: ";
    int initBalance;
    cin >> initBalance;
    
    BankAccount newBA(initBalance);
    BADatabase.push_back(newBA);
    cout << "Created new bank account with ID: " << newBA.getId() << endl;
}

void closeBankAccount() {
    cout << "Bank account ID: ";
    int id;
    cin >> id;
    
    cout << "Are you really sure you want to close bank account with ID: " << id << "? [y/n]";
    char answer;
    cin >> answer;
    
    if (answer == 'y' || answer == 'Y') {
        BADatabase[getIndexFromID(id)].close();
    }
    
    cout << "Bank account with ID: " << id << " is closed!" << endl;
}

void reopenBankAccount() {
    cout << "Bank account ID: ";
    int id;
    cin >> id;
    
    cout << "Are you really sure you want to reopen bank account with ID: " << id << "? [y/n]";
    char answer;
    cin >> answer;
    
    if (answer == 'y' || answer == 'Y') {
        BADatabase[getIndexFromID(id)].reopen();
    }
    
    cout << "Bank account with ID: " << id << " is closed!" << endl;
}

void bankerMode() {
    while(true) {
        cout << "What do you want to do: [0] Exit, [1] List bank accounts, [2] Open bank account, [3] Close bank account, [4] Reopen bank account" << endl;
        
        int value;
        cin >> value;
        
        switch(value) {
            case 0: return;
            case 1: listAllAccounts(); break;
            case 2: openBankAccount(); break;
            case 3: closeBankAccount(); break;
            case 4: reopenBankAccount(); break;
        }
    }
}

void atmMode() {
    cout << "Bank accont ID: ";
    int userId;
    cin >> userId;
    cout << endl;
    
    BankAccount ba = BADatabase[userId];
    if (ba.getCloseStatus()) {
        cout << "Bank account is closed!" << endl;
        return;
    }
    
    while(true) {
        cout << "What do you want to do: [0] Exit, [1] View balance, [2] Deposit money, [3] Withdraw money?" << endl;
        
        int value;
        cin >> value;
        
        switch(value) {
            case 0: return;
            case 1: cout << ba.getBalance() << endl; break;
            case 2: cout << "Amount to deposit: "; cin >> value; ba.deposit(value); break;
            case 3: cout << "Amount to withdraw: "; cin >> value; ba.withdraw(value); break;
        }
    }
}