#include <iostream>

#include "BankSystem.h"

#define MODE_SPACER "------------------------------------------------------------"

using namespace std;

int main()
{
    cout << "Welcome to BA banking system!" << endl << endl;
    while(true) {
        cout << "Mode: Atm/Banker/Exit ";
        char inValue;
        cin >> inValue;
    
        if (inValue == 'e' || inValue == 'E' || inValue == '0') {
            return 0;
        }
    
        if (inValue == 'b' || inValue == 'B') {
            cout << MODE_SPACER << endl;
            bankerMode();
            cout << MODE_SPACER << endl;
        }
        else {
            cout << MODE_SPACER << endl;
            atmMode();
            cout << MODE_SPACER << endl;
        }
    }
    cout << "Goodbye!" << endl << endl;
    
	return 0;
}