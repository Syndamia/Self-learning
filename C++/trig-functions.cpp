#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    const double degr = 360;
    const int func = 4;
    
    double rad = degr * M_PI / 180.0;

    switch(func) {
        case 1: // sin
            printf("sin%.f = %.4f\n", degr, sin(rad));
            break;
        case 2: // cos
            printf("cos%.f = %.4f\n", degr, cos(rad));
            break;
        case 3: // tan
            if (rad == M_PI_2 || rad == 3 * M_PI_2)
                cout << "Division by 0!" << endl;
            else
                printf("tg%.f = %.4f\n", degr, tan(rad));
            break;
        case 4: // cotan
            if (fmod(rad, M_PI) == 0)
                cout << "Division by 0!" << endl;
            else
                printf("cotg%.f = %.4f\n", degr, 1 / tan(rad));
            break;
        default:
            cout << "Wrong function number!" << endl;
            break;
    }
   return 0;
}
