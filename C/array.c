#include <stdio.h>

int main() {
	int row, col;
	float temp;

	// · отпечатване на условието на задачата;
	// · отпечатване на имената на автора на програмата;
	printf("[Task Description Here]\n");
	printf("[Author Here]\n");

	// · въвеждане на входните данни;
	printf("N = ");
	int N = 0;
	scanf("%d", &N);

	printf("Type in values of A:\n");
	float A[N][N];
	for (row = 0; row < N; row++) {
		for (col = 0; col < N; col++) {
			scanf("%f", &A[row][col]);
			while (A[row][col] < -1000 || A[row][col] > 1000) {
				printf("The value must be in [-1000;1000], try another value!\n");
				scanf("%f", &A[row][col]);
			}
		}
	}

	// · отпечатване на входните данни;
	printf("Values of A:\n");
	for (row = 0; row < N; row++) {
		for (col = 0; col < N; col++)
			printf("%f ", A[row][col]);
		printf("\n");
	}

	// a) да се образува едномерен масив C[N], елементите на който са сумата от елементите на съответния ред от масива А;
	float C[N];
	for (col = 0; col < N; col++)
		C[col] = 0;

	for (row = 0; row < N; row++) {
		for (col = 0; col < N; col++)
			C[row] += A[row][col];
	}

	// · отпечатване на получените резултати след обработка а)
	printf("Sums of rows:\n");
	for (col = 0; col < N; col++)
		printf("%f ", C[col]);
	printf("\n");

	// б) полученият масив да се сортира по големина;
	for (col = 0; col < N - 1; col++) { // Bubble sort
		if (C[col] > C[col + 1]) {
			temp = C[col];
			C[col] = C[col + 1];
			C[col + 1] = temp;
		}
	}

	// · отпечатване на получените резултати след обработка б)
	printf("Sorted sums of rows:\n");
	for (col = 0; col < N; col++)
		printf("%f ", C[col]);
	printf("\n");
}

/* Да се състави програма за обработка на масива A[N,N], където данните са реални числа в интервала [-1000;1000]. Програмата да извърши следните действия:
 * · отпечатване на условието на задачата;
 * · отпечатване на имената на автора на програмата;
 * · въвеждане на входните данни;
 * · отпечатване на входните данни;
 *
 * · а) да се образува едномерен масив C[N], елементите на който са сумата от елементите на съответния ред от масива А;
 * · б) полученият масив да се сортира по големина;
 *
 * · отпечатване на получените резултати след обработка а) и след обработка б)
*/
