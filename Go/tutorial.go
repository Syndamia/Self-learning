package main

// import "fmt"
import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

func main() {
	// variablesAndPrinting()
	// input()
	// conditionals()
	// loops()
	// arrays()
	// functions()
	// pointers()
	// structs()
	interfaces()
}

/* Intefaces */

type shape interface {
	area() float64
}

type circle struct {
	radius float64
}

type rect struct {
	width  float64
	height float64
}

func (c circle) area() float64 {
	return math.Pi * c.radius * c.radius
}

func (r rect) area() float64 {
	return r.width * r.height
}

func interfaces() {
	var sh shape = circle{5}
	fmt.Println(sh.area())
	sh = rect{3, 8}
	fmt.Println(sh.area())
}

/* Structs */

type Point struct {
	x int
	y int
}

type Circle struct {
	radius float64
	*Point // struct composition
}

type Person struct {
	name string
	age  int
}

func (p Person) getAge() int {
	return p.age
}

func (p Person) setAgeBroken(newAge int) {
	p.age = newAge
}

func (p *Person) setAgeWorking(newAge int) {
	p.age = newAge
}

func structs() {
	var p1 Point = Point{1, 2}
	p1.x = 5
	fmt.Println(p1.x, p1.y)

	p2 := Point{
		x: 19,
	}
	fmt.Println(p2.x, p2.y) // p2.y is type default

	c1 := Circle{3.14, &Point{30, 40}}
	fmt.Println(c1, c1.x, c1.y)

	person1 := Person{"John", 29}
	fmt.Println(person1.getAge())
	person1.setAgeBroken(19)
	fmt.Println(person1.age)
	person1.setAgeWorking(19)
	fmt.Println(person1.age)
}

/* Pointers */

func pointers() {
	x := 7
	fmt.Println(&x)

	var y *int = &x
	fmt.Println(x, y)
	*y = 10
	fmt.Println(x, y)

	changeValue := func(str *string) {
		*str = "Changed value!"
	}
	str := "Test"
	changeValue(&str)
	fmt.Println(str)
}

/* Functions */

func print(x string) {
	fmt.Println(x)
}

func printMultiple(msg1 string, msg2 string) {
	fmt.Println(msg1, msg2)
}

func printMultiple1(msg1, msg2 string, n1 int) {
	fmt.Println(msg1, msg2, 5)
}

func sum(a, b int) int {
	return a + b
}

func sumAndSub(a, b int) (int, int) {
	return a + b, a - b
}

func sumAndSubAlt(a, b int) (z1 int, z2 int) {
	// Can be simplified just to: z1, z2 = a+b, a-b
	z1 = a + b
	z2 = a - b
	return
}

func sumAndSubAlt1(a, b int) (z1 int, z2 int) {
	defer fmt.Println("function ended") // defer statements execute when a function finishes
	z1, z2 = a+b, a-b
	return
}

func functions() {
	print("Hello World")
	printMultiple("One", "Two")
	printMultiple1("One", "Two", 5)
	fmt.Println(sum(1, 2))
	fmt.Println(sumAndSub(1, 2))
	fmt.Println(sumAndSubAlt(1, 2))
	fmt.Println(sumAndSubAlt1(1, 2))

	test := func() {
		fmt.Println("test variable")
	}
	test()

	// Saves a function and execution of it, so it saves what the function returns if given
	// the number 9
	test1 := func(x int) int {
		return x * 2
	}(9)
	fmt.Println(test1)

	test3 := func(f func()) {
		f()
	}
	test3(test)

	func() {
		fmt.Println("woah")
	}()

	returnFunc := func(id int) func(int) int {
		switch id {
		case 1:
			return func(n int) int { return n * 2 }
		case 2:
			return func(n int) int { return n * 3 }
		default:
			return func(n int) int { return n * -1 }
		}
	}
	fmt.Println(returnFunc(2)(5))
}

/* Arrays */

func arrays() {
	var arr [5]int // fills with default value
	arr[2] = 5
	fmt.Println(arr) // properly prints whole array

	arr2 := []int{1, 2, 3}
	arr3 := [5]int{1, 2, 3}
	fmt.Println(arr2, arr3, len(arr2))

	arr2d := [][]int{{1, 2}, {10, 20}}
	fmt.Println(arr2d)

	bigarr := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	smallerArr := bigarr[:]
	fmt.Println(smallerArr)
	smallerArr1 := bigarr[4:]
	fmt.Println(smallerArr1)
	smallerArr2 := bigarr[:2]
	fmt.Println(smallerArr2)
	smallerArr3 := bigarr[4:6] // start index:end index
	fmt.Println(smallerArr3)

	var a []int = []int{1, 2, 3}
	b := append(a, 4, 5, 6)
	fmt.Println(b)

	for k, elem := range bigarr { // equivalent to: for k := 0; k < len(bigarr); k++
		fmt.Println(k, elem)
	}

	for _, elem := range bigarr { // equivalent to this C# code: foreach(var elem in bigarr)
		fmt.Println(elem)
	}

	// Maps are the Go equivalent of C# Dictionaries
	mp := map[string]int{
		"apple":  1,
		"pear":   2,
		"orange": 9,
	}
	fmt.Println(mp)
	fmt.Println(mp["orange"])
	delete(mp, "orange")
	fmt.Println(mp)
	val, exists := mp["adf"] // If value exists at key, val is assigned to it and exists is true, otherwise val is default for type and exists is false
	fmt.Println(exists, val)
}

/* Loops */

func loops() {
	i = 0
	for i < 5 {
		fmt.Println(i)
		i++
	}

	for {
		if i == 8 {
			break
		} else if i%2 == 0 {
			fmt.Println(i)
		}
		i++
	}

	for j := 0; j < 5; j++ {
		fmt.Println(j)
	}
}

/* Conditionals */

func conditionals() {
	age := 21
	if driving_age := 18; age < driving_age {
		fmt.Println("You cannot drive!")
	} else if age < 16 {
		fmt.Println("You cannot use this app!")
	} else {
		fmt.Println("You're good")
	}

	n := 5
	switch n {
	case 1:
		fmt.Println("Very cool")
	case 4:
		fmt.Println("Not cool")
	default:
		fmt.Println("Bruh")
	}

	switch {
	case n > 3:
		fmt.Println("Yay")
	default:
		fmt.Println("Nay")
	}
}

/* User input */

func input() {
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Printf("Type age: ")
	scanner.Scan()

	userInput := scanner.Text() // Always string, duuh
	// Variable, base (binary, octal, decimal, ..), integer size (8, 16, 32, 64, ..)
	// _ is an anonymous variable, so it doesn't get anywhere. Second returned variable is error
	// Bitsize 0 is normal int
	age, _ := strconv.ParseInt(userInput, 10, 0)

	fmt.Printf("You were born in %d!", 2021-age)

}

/* Variables and printing */

var i int // global variable

func variablesAndPrinting() {
	var msg string
	msg = "Hello World!"

	test := 5.98
	var num int = int(test)
	num += 4
	num++

	str := "Yoo"
	var str1 = "Bruh"

	//var v1, v2, v3 int = 45, 2342, 6634
	//v4, v5, v6 = 6653, true, "nafd"

	fmt.Println(msg, num) // Prints both with space in between
	fmt.Print(str, str1)
	fmt.Println()
	/* %T - type
	* %v - value
	* %% - escape % character
	* %t - prints bool
	* %b, %o, %d, %x - format int as base 2, 8, 10 and 16 repsectively
	* %e, %f, %g - format float as scientific notation, decimal no exponent (cuts of number) or large exponent (full number) respectively
	* %f - default width, default precision; %9f - width of 9, default precision; %.2f - default width, precision 2; %9.f - width of 9, precision 0; %9.2f - width of 9, precision 2;
	  - width forces certain string length (use negative number to add spaces to the right), it works with %s and %q also (%-9q)
	* %d - pads a number with a given digit, so %07d with force width of 7 and add 0s to the number
	* %s, %q - show string or show string with quotes
	*/
	fmt.Printf("%T", num) // Prints type
	out := fmt.Sprintf("%v %b", str, num)
	fmt.Println(out)
}
