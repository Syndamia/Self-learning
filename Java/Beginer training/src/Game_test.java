import java.util.Arrays;
import java.io.*;
import java.text.*;

public class Game_test {
    public static final char[][] ORIGINAL_MAP = {
            {' ', '-', '-', '-', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '[', ' ', '|', ' '},
            {' ', '-', '<', '-', ' '}
    };

    public static void main(String args[]) {
        Console cons = System.console();

        while (true){
            ReWriteMap();
            System.out.println("Type direction and press enter:");
            String direction = cons.readLine();

            switch (direction.charAt(0)) {
                case 'w': Player.yPos++; break;
                case 's': Player.yPos--; break;
                case 'a': Player.xPos--; break;
                case 'd': Player.xPos++; break;
            }

            try { Thread.sleep(1000); }
            catch (Exception ex) { }
        }
    }

    public static void ReWriteMap(){
        var map = Arrays.copyOf(ORIGINAL_MAP, ORIGINAL_MAP.length);

        for (int i = 0; i < map.length; i++){
            for (int j = 0; j < map[0].length; j++){
                if (i == Player.yPos && j == Player.xPos) map[i][j] = Player.body;

                System.out.print(map[i][j]);
            }
            System.out.println();
        }
    }
}

class Player {
    public static char body = '*';

    public static int yPos = 0, xPos = 1; //y is up/down ; x is left/right
}
