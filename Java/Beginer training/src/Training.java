import java.util.ArrayList;
import java.math.*;

public class Training {
    public static void main(String args[]){
        //PrintFibonacci(7);
        System.out.println(NumberName(6783508));
    }

    public static String NumberName(int num) {
        StringBuilder name = new StringBuilder(num < 0 ? "negative " : "");
        var number_parts = new ArrayList<Integer>();

        while (num != 0) {
            number_parts.add(Math.abs(num % 1000));
            num /= 1000;
        }

        for (int i = number_parts.size() - 1; i > -1; i--){
            int curr_num = number_parts.get(i);

            if ((curr_num + "").length() == 3) {
                name.append(SmallNumName(curr_num / 100)).append(" hundred ");

                curr_num %= 100;
                if (curr_num / 10 == 0) name.append("and ");
            }
            name.append(SmallNumName(curr_num));

            switch (i) {
                case 1: name.append(" thousand "); break;
                case 2: name.append(" million "); break;
                case 3: name.append(" milliard "); break;
            }
        }
        return name.toString();
    }

    private static String SmallNumName(int num) {
        int part = 1;
        switch (num) {
            case 0: return "";
            case 1: return "one";
            case 2: return "two";
            case 3: return "three";
            case 4: return "four";
            case 5: return "five";
            case 6: return "six";
            case 7: return "seven";
            case 8: return "eight";
            case 9: return "nine";

            case 10: return "ten";
            case 11: return "eleven";
            case 12: return "twelve";
            case 13: return "thirteen";
            case 15: return "fifteen";
            case 18: return "eighteen";
            case 14:                part = 4;
            case 16: if (part == 1) part = 6;
            case 17: if (part == 1) part = 7;
            case 19: if (part == 1) part = 9;
                     return SmallNumName(part) + "teen";
        }

        if (num > 20 && num < 100) {
            int f_digit = num / 10;
            StringBuilder name = new StringBuilder();

            switch (f_digit) {
                case 2: name.append("twenty "); break;
                case 3: name.append("thirty "); break;
                case 5: name.append("fifty "); break;
                case 8: name.append("eighty "); break;

                default: name.append(SmallNumName(f_digit)).append("ty "); break;
            }

            return name + SmallNumName(num % 10);
        }
        return "ERR";
    }

    public static void PrintFibonacci (int length) {
        int a = 1, b = 0;
        for (int i = 0; i < length; i++){
            System.out.printf("%d ", a);

            int temp = a;
            a += b;
            b = temp;
        }
    }
}
