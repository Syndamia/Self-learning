import java.util.ArrayList;
import java.util.Scanner;

public class Program {

    public static String[] digits = {"", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
    public static String[] teens = {"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
    public static String[] tens = {"ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};

    public static void main(String args[]){
        //Used http://www.englishlessonsbrighton.co.uk/saying-large-numbers-english/ and https://www.grammarly.com/blog/hyphen-in-compound-numbers/
        Scanner scn = new Scanner(System.in);

        System.out.println("Number to British English text");
        System.out.println("How to use: type a whole number, press enter and the program will say it's full proper name. Close the program to stop it.");
        System.out.println("NOTE: Your numbers must be between 9223372036854775807 and –9223372036854775808 (any characters that are not numbers and - will result in error)");

        while (true) {
            try {
                long n = Long.parseLong(scn.nextLine().trim());
                System.out.println(NumberName(n));
            } catch (Exception ex) {
                System.out.println("ERROR: Please type a whole number between 2147483647 and -2147483648 (NOTE: any characters that are not numbers and - will result in this error)");
            }
        }
    }

    public static String NumberName(long num) {
        StringBuilder name = new StringBuilder(num < 0 ? "negative " : "");
        var number_parts = new ArrayList<Long>();

        if (num == 0) return "zero";

        while (num != 0) {
            number_parts.add(Math.abs(num % 1000));
            num /= 1000;
        }

        for (int i = number_parts.size() - 1; i > -1; i--){
            long curr_num = number_parts.get(i);

            if ((curr_num + "").length() == 3) {
                name.append(SmallNumberName((int)curr_num / 100)).append(" hundred");

                curr_num %= 100;
                if (curr_num != 0) name.append(" and ");
            }
            name.append(SmallNumberName((int)curr_num));

            boolean to_determine = true;
            switch (i) {
                case 6:
                    if (number_parts.get(6) != 0) { //to_determine is always true when it reaches here
                        name.append(" trillion");
                        to_determine = false;
                    }
                case 5:
                    if (number_parts.get(5) != 0 && to_determine) {
                        name.append(" billiard");
                        to_determine = false;
                    }
                case 4:
                    if (number_parts.get(4) != 0 && to_determine) {
                        name.append(" billion");
                        to_determine = false;
                    }
                case 3:
                    if (number_parts.get(3) != 0 && to_determine) {
                        name.append(" milliard");
                        to_determine = false;
                    }
                case 2:
                    if (number_parts.get(2) != 0 && to_determine) {
                        name.append(" million");
                        to_determine = false;
                    }
                case 1:
                    if (number_parts.get(1) != 0 && to_determine) {
                        name.append(" thousand");
                        to_determine = false;
                    }

                    if (number_parts.subList(0, i).stream().anyMatch(n -> n != 0) && !to_determine) {
                        if (number_parts.get(0) / 100 == 0) name.append(" and ");
                        else name.append(", ");
                    }
                    break;
            }
        }
        return name.toString();
    }

    private static String SmallNumberName(int num) {
        if (num < 20) {
            try {
                return digits[num];
            } catch (Exception e) {
                return teens[num % 10];
            }
        } else if (num < 100) {
            return num % 10 == 0 ? tens[(num / 10) - 1] : tens[(num / 10) - 1] + "-" + SmallNumberName(num % 10);
        }
        return "ERR";
    }
}