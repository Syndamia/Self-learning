<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <?php
            $name = "John";
            include "header.php";
        ?>
        <br>
        <?php
            $loggedin = false;

            if ($loggedin) {
                echo "Hello";
            }
            else {
                echo "Please log in";
            }
        ?>

        <form class="" action="process.php" method="post">
            Enter your name: <input type="text" name="name" value="">
            <input type="submit" name="" value="Submit">
        </form>

        <?php
            function doStuff($workers) {
                echo "First worker: " . strtolower($workers[0]) . strlen($workers[0]) . " " . strtoupper($workers[1]) . $workers[0][0] . " <br>";
                echo str_replace("Jo", "Mo", $workers[0]) . "<br>";
                echo substr($workers[0], 1, 1) . "<br>";
            }

            $staff = array("John", "Bob");
            doStuff($staff);

            foreach ($workers as $person) {
                echo $person . ", ";
            }
        ?>

        <br>

        <?php
            $numbers = array(1, 2, 3, 4);
            $sum = 0;

            foreach($numbers as $num) {
                $sum += $num;
            }
            echo $sum . "<br>";

            $num = 0;
            echo $num++;
            echo $num;

            $num = 0;
            echo ++$num;
        ?>

        <br>

        <h2>Calculator</h2>

        <form class="" action="calc.php" method="post">
            <label for=""></label>
            <br>
            <input type="submit" name="one" value="1">
            <input type="submit" name="two" value="2">
            <input type="submit" name="three" value="3">
            <br>
            <input type="button" name="four" value="4">
            <input type="button" name="five" value="5">
            <input type="button" name="six" value="6">
            <br>
            <input type="button" name="seven" value="7">
            <input type="button" name="eight" value="8">
            <input type="button" name="nine" value="9">
            <br>
            <input type="button" name="zero" value="0">
            <br>
            <input type="button" name="add" value="+">
            <input type="button" name="sub" value="-">
            <input type="button" name="mlt" value="*">
            <input type="button" name="div" value="/">

        </form>

        <br>

        <form class="" action="index.php" method="post">
            Blue: <input type="checkbox" name="colors[]" value="blue"> <br>
            Red: <input type="checkbox" name="colors[]" value="red"> <br>
            Yellow: <input type="checkbox" name="colors[]" value="yellow"> <br>
            <input type="submit" name="" value="Submit">
        </form>

        <?php
            // comment
            foreach($_POST["colors"] as $color) {
                echo $color . " ";
            }
        ?>

        <br>

        <?php
            $ages = array("John" => 31, "Alex" => 20);

            echo $ages["John"] . $ages["Alex"];
        ?>

        <br>

        <?php
            class Person {
                public $name;
                private $age;

                function __construct($personName, $personAge) {
                    $this->name = $personName;
                    $this->setAge($personAge);
                }

                function setAge($age) {
                    if ($age < 0 || $age > 200) {
                        throw new Exception("Invalid age!", 1);
                    }
                    $this->age = $age;
                }

                function getAge() {
                    return $this->age;
                }
            }

            class Worker extends Person {

            }

            $max = new Worker("Alex", 34);

            //$max->name = "Max";
            //$max->age = 22;

            echo $max->name, $max->getAge();
        ?>

        <?php
            include "footer.html";
        ?>
    </body>
</html>
