#!/usr/bin/perl

=begin Description
Program that greets a person
=cut

$name = 'John';
print <<"EOF";
Hello, $name!
How are you today?
EOF

=begin Data_types
Scalar - $
number (Integer, Negative Integer, Floating point, Scientific notation, Hexadecimal, Octal), 
string (\n - newline, \t - horizontal tab, ...), 
reference

Array - @

Hash = %
key/value pairs
=cut

$name = "John";
$age  = 30.5;

@friends = ("Bob", "Jeff", "Mark");

%friend_age = ("Bob", 20, "Jeff", 30, "Mark", 21);

print(<<"END_OF_MESSAGE");
    Using a here-document:

    $name is $age years old.
    His friends are $friends[0], $friends[1], $friends[2].
    $friends[0] is $friend_age{$friends[0]} years old.
END_OF_MESSAGE

print "Using a multi line string:

    $name is $age years old.
    His friends are $friends[0], $friends[1], $friends[2].
    $friends[0] is $friend_age{$friends[0]} years old.

";

$can_have_pension = ($age >= 65) ? "$name can have can have pension" : "$name is too young" ; 
print $can_have_pension . "\n";

if( $name == "John" ) {
    # executes if true
    $name = "Johnny";
} elsif( $name == "Bobby" ) {
    $name = "Bob";
} else {
    $name = "Johnathan";
}

unless( $name == "Mark" ) {
    # executes if false
    $name = "John";
}

for ($i = 0; i < 10; i++) {}

@nums = (1..10);

foreach @i (@nums) {}

$a = 14.464;
$b = 30;

print $a . $b . "\n";

print "File name: " . __FILE__ . " ; Line number: " . __LINE__ . " ; Package: " . __PACKAGE__ . "\n";

@days = qw/Mon Tue Wed/;
@days[5] = 'Sat';
@days[4] = 'Fr';
push(@days, 'Sun');

@weekend = @days[5..6];

@one_to_ten = (1..10);

$size = @days;
print @days;
print @weekend;
print 'scalar: ' . scalar @days . ' ; $: ' . $size . ' ; $#: ' . $#days . "\n";
