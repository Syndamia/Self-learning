lower = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
upper = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

while True:
    key = abs(int(input("Type a key (0 - 26): ")))
    while key > 26:
        key -= 26

    msg = input("Type a message: ")
    decoded = [] ; encoded = []

    for c in msg:
        try:
            decoded.append(lower[lower.index(c) - key])
            encoded.append(lower[lower.index(c) + key])
        except:
            try:
                decoded.append(upper[upper.index(c) - key])
                encoded.append(upper[upper.index(c) + key])
            except:
                decoded.append(c)
                encoded.append(c)

    print(end="Decoded message: ")
    for c in decoded:
        print(end=c)
    print()

    print(end="Encoded message: ")
    for c in encoded:
        print(end=c)
    print("\n")