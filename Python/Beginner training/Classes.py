import sys
import math
import random
import os

def BankAccountTask():
    class BankAccount:
        def __init__(self, id, balance):
            self.id = int(id)
            self.__balance = float(balance)

        def Deposit(self, amount):
            self.__balance += amount

        def Withdraw(self, amount):
            self.__balance -= amount

        def GetBalance(self):
            return self.__balance

        def __str__(self):
            return "Account {0}, balance {1}".format(self.id, self.GetBalance())

    accounts = {}
    while True:
        command = input().split(' ')

        if command[0].lower() == "end":
            break
        elif command[0] == "Add":
            accounts[int(command[1])] = BankAccount(int(command[1]), float(command[2]))
        elif command[0] == "Deposit":
            accounts[int(command[1])].Deposit(float(command[2]))
        elif command[0] == "Withdraw":
            accounts[int(command[1])].Withdraw(float(command[2]))
        elif command[0] == "Info":
            print(accounts[int(command[1])])
        elif command[0] == "Print":
            for acc in accounts: print(accounts[acc])
        else:
            print("Invalid command!")


def PizzaTask():
    class Dough():
        def __init__(self, type, cooking_method, weight):
            type = type.lower(); cooking_method = cooking_method.lower()
            self.__validate(type, cooking_method, weight)

            self.__type = str(type)
            self.__cooking_method = str(cooking_method)
            self.__weight = int(weight)

            self.__total_cals = 2.0 * self.__weight
            if type == "white": self.__total_cals *= 1.5
            if cooking_method == "crispy":
                self.__total_cals *= 0.9
            elif cooking_method == "chewy":
                self.__total_cals *= 1.1

        def __validate(self, type, cooking_method, weight):
            type = str(type); cooking_method = str(cooking_method); weight = int(weight)

            if (type != "white" and type != "wholegrain") or \
                    (cooking_method != "crispy" and cooking_method != "chewy" and cooking_method != "homemade"):
                raise ValueError()

            if weight < 1 or weight > 200:
                raise NameError()

        def get_cals(self):
            return self.__total_cals

    class Topping():
        def __init__(self, type, weight):
            type = type.lower()
            self.__validate(type, weight)

            self.__type = str(type)
            self.__weight = int(weight)

            self.__total_cals = 2.0 * self.__weight
            if type == "veggies":
                self.__total_cals *= 0.8
            elif type == "sauce":
                self.__total_cals *= 0.9
            elif type == "cheese":
                self.__total_cals *= 1.1
            else:
                self.__total_cals *= 1.2  # Sauce

        def __validate(self, type, weight):
            type = str(type); weight = int(weight)

            # usng different types of error to indicate what to write on screen
            if type != "meat" and type != "veggies" and type != "cheese" and type != "sauce":
                raise AssertionError()

            if weight < 1 or weight > 50:
                raise AttributeError()

        def get_cals(self):
            return self.__total_cals

    class Pizza():
        def __init__(self, name, num):
            self.__validate(name, num)

            self.name = str(name)
            self.__dough = None  # not public because task doesn't want it to be
            self.__toppings = []
            self.__total_cals = 0.0

        def __validate(self, name, num):
            name = str(name); num = int(num)

            if len(name) < 1 or len(name) > 15:
                raise MemoryError()

            if num < 0 or num > 10:
                raise IndexError()

        def add_dough(self, dough):
            self.__dough = dough
            self.__total_cals += dough.get_cals()

        def add_topping(self, topping):
            self.__toppings.append(topping)
            self.__total_cals += topping.get_cals()

        def __str__(self):
            return "{0} - {1} Calories.".format(self.name, self.__total_cals)

        def n_toppings(self):
            return len(self.__toppings)

        def cals(self):
            return self.__total_cals

    info = input().split(' ')
    try:
        in_pizza = Pizza(info[1], info[2])

        info = input().split(' ')
        in_pizza.add_dough(Dough(info[1], info[2], info[3]))

        while True:
            info = input().split(' ')

            if info[0].lower() == "end":
                break
            else:
                in_pizza.add_topping(Topping(info[1], info[2]))

        print(in_pizza)

    except ValueError:
        print('Invalid type of dough.')
    except NameError:
        print('Dough weight should be in the range [1..200].')
    except AssertionError:
        print('Cannot place {0} on top of your pizza.'.format(info[1]))
    except AttributeError:
        print('{0} weight should be in the range [1..50].'.format(info[1]))
    except MemoryError:
        print('Pizza name should be between 1 and 15 symbols.')
    except IndexError:
        print('Number of toppings should be in range [0..10].')

    while True:
        command = input().split(' ')

        if command[0] == "END": break
        # elif command[0] == "Dough":


def CarVender():
    class Engine():
        def __init__(self, model, power, displacement = 'n/a', efficiency = 'n/a'):
            self.__model = model
            self.__power = power
            self.displacement = displacement
            self.efficiency = efficiency

        def __str__(self):
            return "  {0}:\n    Power: {1}\n    Displacement: {2}\n    Efficiency: {3}"\
                    .format(self.__model, self.__power, self.displacement, self.efficiency)

    class Car():
        def __init__(self, model, engine, weight = 'n/a', color = 'n/a'):
            self.__model = model
            self.__engine = engine
            self.weight = weight
            self.color = color

        def __str__(self):
            return "{0}:\n{1}\n  Weight: {2}\n  Color: {3}"\
                    .format(self.__model, self.__engine, self.weight, self.color)

    engines = {}
    engines_n = int(input())
    for i in range(engines_n):
        info = input().split(' ')
        list(filter(lambda e: e != ' ', info))

        engines[info[0]] = (Engine(info[0], info[1]))
        if len(info) == 3:
            try:
                engines[info[0]].displacement = int(info[2])
            except:
                engines[info[0]].efficiency = info[2]
        elif len(info) == 4:
            engines[info[0]].displacement = int(info[2])
            engines[info[0]].efficiency = info[3]

    cars = []
    cars_n = int(input())
    for i in range(cars_n):
        info = input().split(' ')
        info = [e for e in info if e != '']

        cars.append(Car(info[0], engines[info[1]]))
        if len(info) == 3:
            try:
                cars[i].weight = int(info[2])
            except:
                cars[i].color = info[2]
        elif len(info) == 4:
            cars[i].weight = info[2]
            cars[i].color = info[3]

    for curr_car in cars:
        print(curr_car)


#BankAccountTask()
#PizzaTask()
CarVender()
