import sys
import math
import random
import os

def GoSkiing():
    skier_n = int(input('Type number of skiers: '))
    jacket_n_per_person = int(input('Type number of jackets per person: '))
    helmet_n_per_person = int(input('Type number of helmets per person: '))
    shoe_set_n_per_person = int(input('Type number of shoes per person: '))

    total_per_person = (jacket_n_per_person * 120) + (helmet_n_per_person * 75) + (shoe_set_n_per_person * 299.9)
    total_price = total_per_person * skier_n

    print('Total price for all skiers: ', round(total_price + total_price * 0.2, 2))

def Pets():
    days_absent = int(input('Type the number of days that you wil be absent: '))
    left_food_kg = float(input('Type the number of left food in kg: '))
    daily_consumption_first = float(input('Type the daily consumption of the first cat in kg: '))
    daily_consumption_second = float(input('Type the daily consumption of the second cat in kg: '))

    left_food_kg -= days_absent * daily_consumption_first
    left_food_kg -= days_absent * daily_consumption_second

    if left_food_kg >= 0:
        print('The cats are well fed')
        print('{} kilos of food left'.format(left_food_kg))
    else:
        print('The cats are hungry')
        print('{} more kilos of food are needed'.format(int(abs(left_food_kg))))

def Hotel():
    n_nights = int(input('Type number of nights: '))
    room_type = input('Type the type of room: ')

    price = 0.0
    if room_type == 'apartment':
        price = n_nights * 70
    else:
        price = n_nights * 125

    # code doesn't account for writing mistakes
    if n_nights < 10:
        if room_type == 'apartment':
            price *= 0.7
        else:
            price *= 0.9
    elif n_nights > 15:
        if room_type == 'apartment':
            price *= 0.5
        else:
            price *= 0.8
    else:
        if room_type == 'apartment':
            price *= 0.65
        else:
            price *= 0.85

    print('Total price: {}'.format(round(price, 2)))

def TakingCare():
    food = int(input('Type bought amount of food in kg: ')) * 1000
    days_care = int(input('Type days the the animals wil be taken care of: '))

    for i in range(days_care):
        daily_consumption = int(input('Food that the animal has eaten that day: '))
        food -= daily_consumption

    if food >= 0:
        print('Food is enough! Leftovers: {} grams'.format(food))
    else:
        print('Food is not enough. You need {} grams more'.format(abs(food)))

GoSkiing()
Pets()
Hotel()
TakingCare()