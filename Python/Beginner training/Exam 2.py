import sys
import math
import random
import os

def FitnessEquipment():
    total_sum = 0.0
    for x in range(int(input())):
        item_name = input()

        if item_name == "treadmill":
            total_sum += 5899
        elif item_name == "cross trainer":
            total_sum += 1699
        elif item_name == "exercise bike":
            total_sum += 1789
        elif item_name == "dumbells":
            total_sum += 579
        else:
            print("ERROR, Invalid product name")

    print(round(total_sum, 2))

def FitnessVisitors():
    names = list(input("Type names: ").split(", "))

    while True:
        command = input()
        if command == "END": break

        if command == "Add visitor":
            names.append(input())
        elif command == "Add visitor on position":
            name_to_insert = input()
            names.insert(int(input()), name_to_insert)
        elif command == "Remove visitor on position":
            names.pop(int(input()))
        elif command == "Remove last visitor":
            names.pop(len(names) - 1)
        elif command == "Remove first visitor":
            names.pop(0)
        else:
            print("ERROR, indxistant command")

    print(names)

FitnessEquipment()
FitnessVisitors()