import sys
import math
import random
import os
import keyboard
import time

ORIGINAL_MAP = [[' ', '>', '-', '-', '-', '-', ' '],
                [' ', '|', ' ', ' ', ' ', '|', ' '],
                [' ', '[', ' ', ' ', ' ', '|', ' '],
                [' ', '-', '-', '-', '=', '-', ' ']]
map = ORIGINAL_MAP[:]

player = '*'
xPos = 1; yPos = 0

def ReWriteScreen():
    map = ORIGINAL_MAP[:]

    for i in range(len(ORIGINAL_MAP)):
        for j in range(len(ORIGINAL_MAP[0])):
            if i == yPos: map[yPos][xPos] = player

            print(end=map[i][j])
        print()

while True:
    try:
        if keyboard.is_pressed('w') and yPos > 0:
            yPos -= 1
        elif keyboard.is_pressed('s') and yPos < 3:
            yPos += 1
        elif keyboard.is_pressed('a') and xPos > 1:
            xPos -= 1
        elif keyboard.is_pressed('d') and xPos < 5:
            xPos += 1
    except:
        continue

    ReWriteScreen()
    time.sleep(1)