import sys
import os
import random
import math

def Divide():
    cycle_n = int(input("Type number of numbers that will be typed: "))
    p1 = 0; p2 = 0; p3 = 0

    for x in range(cycle_n):
        num = int(input())

        if num % 2 == 0: p1 += 1
        if num % 3 == 0: p2 += 1
        if num % 4 == 0: p3 += 1

    print(round(p1 * 100 / cycle_n, 2), "%")
    print(round(p2 * 100 / cycle_n, 2), "%")
    print(round(p3 * 100 / cycle_n, 2), "%")


def Histogram():
    cycle_n = int(input("Type number of numbers that will be typed: "))
    p1 = 0; p2 = 0; p3 = 0; p4 = 0; p5 = 0

    for x in range(cycle_n):
        num = int(input())

        if num < 200:
            p1 += 1
        elif num < 400:
            p2 += 1
        elif num < 600:
            p3 += 1
        elif num < 800:
            p4 += 1
        else:
            p5 += 1

    print(round(p1 * 100 / cycle_n, 2), "%")
    print(round(p2 * 100 / cycle_n, 2), "%")
    print(round(p3 * 100 / cycle_n, 2), "%")
    print(round(p4 * 100 / cycle_n, 2), "%")
    print(round(p5 * 100 / cycle_n, 2), "%")


def NumberMatrix():
    size = int(input("Type size of matrix: "))

    for x in range(size):
        for y in range(size):
            num = y + x + 1

            if num <= size or x == 0:
                print(end="{} ".format(num))  # doing it like this for practice
            else:
                print(end="{} ".format(2 * size - num))  # doing it like this for practice
        else:
            print()


def Factoriel():
    factoriel_n = int(input("Type times of factoriel: "))

    final_int = 1
    for x in range(1, factoriel_n + 1):
        final_int *= x

    print(final_int)


def HappyNumber():
    while True:
        start = int(input("Type a start number: "))

        try:
            num = start
            seen = [num]

            while num != 1:

                temp = 0
                for c in str(num):
                    temp += math.pow(CharToNum(c), 2)
                num = int(temp)

                if seen.count(num) > 0: raise()
                seen.append(num)

            print("{0} is a happy number.".format(start))
        except:
            print("{0} isn't a happy number.".format(start))


def CharToNum(char):
    if char == "0": return 0
    elif char == "1": return 1
    elif char == "2": return 2
    elif char == "3": return 3
    elif char == "4": return 4
    elif char == "5": return 5
    elif char == "6": return 6
    elif char == "7": return 7
    elif char == "8": return 8
    elif char == "9": return 9

#Divide()
#Histogram()
#NumberMatrix()
#Factoriel()
HappyNumber()
