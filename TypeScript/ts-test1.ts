class Person {
	private _name: string;

	set name(value: string) {
		if (value == "Pesho")
			throw new Error("Nah fam");
		this._name = value;
	}

	get name() {
		return this._name;
	}

	constructor(name: string) {
		this.name = name;
	}
}
